<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';




class Acesso extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

      error_reporting(0);
    }
   function index_get(){
    echo 'Method inicial';
   }
   public function verificarUsuario_post(){
          if(  !empty($resultado = $this->VerificaCadastro($this->post('email') ) )){
                       
                          $this->response([$resultado], REST_Controller::HTTP_OK);    
                       
          }else{
            $this->response([array('status' =>'00' )], REST_Controller::HTTP_CONTINUE);   
          }
   }


   public function cadastrarUsuario_post() {
    
        if(!empty($this->post('nome')) and !empty($this->post('email')) and !empty($this->post('senha')) and !empty($this->post('token')) ){
            $resultado = $this->Cadastro($this->post('nome'),$this->post('email'),$this->post('senha'),$this->post('token'));
             if($resultado['resultDados'] === 2){
                  
                  $this->response([array('status' =>'2')], REST_Controller::HTTP_OK);    
             }else{
                if($resultado['resultDados'] === 1){
                  $this->response([array('status' =>'1')], REST_Controller::HTTP_CONFLICT);       
                }else{
                  $this->response([array('status' =>'0')], REST_Controller::HTTP_BAD_REQUEST);   
                }
                     
                  
 
             }
       
       }else{
            $this->response([array('status' =>'00')], REST_Controller::HTTP_BAD_REQUEST);   
       }
              
   }
   
   public function login_post(){
    $resultado = $this->login($this->post('login'), $this->post('senha'));
    
    if($resultado[0]['status'] ===1){
      $this->response([$resultado], REST_Controller::HTTP_OK);  
    }else{
        $this->response([$resultado], REST_Controller::HTTP_CONFLICT);  
    }
    
   }

   function auth(){
   
      if(!empty($this->post('t1')) ){
              $resultado = $this->VerificaTokenAcesso($this->post('t1'));
              $this->response([array('status' =>'1' )], REST_Controller::HTTP_OK);    
        
       }
        
    }
}